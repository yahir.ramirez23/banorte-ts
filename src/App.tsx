import { useDispatch, useSelector } from "react-redux";
import { RootState } from "./redux/reducers/rootReducer";
import {
	addAction,
	substractAction,
	resetAction,
} from "./redux/actions/postActions";

import Router from "./router";
import Navigator from "./components/Nav";
import "./App.css";

function App() {
	const dispatch = useDispatch();
	const { counter } = useSelector((state: RootState) => state.numberCount);

	const suma = () => {
		dispatch(addAction(20));
	};

	const resta = () => {
		dispatch(substractAction(10));
	};

	const resetear = () => {
		dispatch(resetAction(0));
	};

	return (
		<div className="App">
			<header style={{ background: "red", color: "white" }}>
				<h1> Contador: {counter} </h1>
				<button onClick={suma}>Sumar</button>
				<button
					onClick={() => {
						resta();
					}}>
					Restar
				</button>
				<button
					onClick={() => {
						resetear();
					}}>
					Resetear
				</button>
				<Navigator />
			</header>
			<Router />
		</div>
	);
}

export default App;
