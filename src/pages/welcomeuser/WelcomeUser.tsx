
import {useNavigate} from 'react-router-dom'

import RequirimentsOptions from '../../components/requerimentoptions'
import styles from './welcomeuser.module.scss';
import ContinueButton from '../../components/continuebutton';
export default function WelcomeUser() {
    const navigate=useNavigate()

    const textRequiriments=[
    "Acta constitutiva de la empresa",
    "Comprobante de domicilio de la empresa",
    "Firma electrónica de la empresa/e.firma",
    "Cédula de Identiicación Fiscal y constancia de la Firma Electronica Avanzada",
    "Teleóno y correo electrónico de los apoderados legales",
    "INE/IFE vigente de los apodrerados legales que funge como comprobante de domicilio ",
    "Teléfono móvil con cámara(timaremos una foto y un video)"]
  return (
    <>
      <div className="container mx-auto px-4 ">
        <div>
          <div>
            <img
              className="mx-auto h-12 w-auto"
              src="https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg"
              alt="Workflow"
            />
            <h2 className="text-lg font-bold mt-5 text-gray-700">Bienvenido al módulo de contratación de cuentas empresariales</h2>
          </div>
         <div className={`rounded-lg mt-5 shadow-inner ${styles['background_container']}`}> 
         <h2 className="text-gray-700 font-bold pt-5" >Para la contratación  requerirás:</h2>
         <div className="flex justify-center p-5">
         <div className="grid grid-cols-2 text-left w-2/3">
            {textRequiriments.map((text,index)=> <RequirimentsOptions key={index} text={text}/>)}
        </div>
         </div>
         </div>
         <footer>
            <ContinueButton isDisabled={false}  nextPage={()=>navigate('/companydata')} />
         </footer>
        </div>
      </div>
    </>
  )
}