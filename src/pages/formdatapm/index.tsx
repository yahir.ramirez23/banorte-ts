
import {useState} from 'react';
import BackButton from '../../components/backbutton'
import ContinueButton from "../../components/continuebutton";
import styles from "./formdatapm.module.scss";
import {useNavigate} from 'react-router-dom'
import Layout from '../../layout'
import ComponentData from "./componentData";
import Modal from '../../components/modal';
import RenderComponent from '../../common/RenderComponent';
import OutlineButton from '../../components/outlinebutton';

function FormDataPM() {
	const { components, elements } = ComponentData;
    const navigate = useNavigate();
	let [isOpen, setIsOpen] = useState(false)

	function closeModal() {
	  setIsOpen(false)
	}
  
	function openModal() {
	  setIsOpen(true)
	}

    return (<Layout>
        		<BackButton  goBack={() => navigate('/')}/>
        		<h2 className="font-bold">Ingresa tus datos</h2>
        		<section className={styles["holder"] }>
					<div className="mt-5">
						{components.map((component) =>
							component.idPregunta !== 0 ? (							
								<RenderComponent item={component}></RenderComponent>
							) : (
								<section
									className={`grid grid-cols-${component.columns} gap-4`}>
									
									{component.children?.map((item) => <RenderComponent item={item}></RenderComponent>)}
								</section>
							)
						)}
					</div>
                    <p>Datos de contacto</p>
                    <div className="mt-5">
						{elements.map((component) =>
							component.idPregunta !== 0 ? (
								<RenderComponent item={component}></RenderComponent>
							) : (
								<section
									className={`grid grid-cols-${component.columns} gap-4`}>
									{component.children?.map((item) => <RenderComponent item={item}></RenderComponent>)}
								</section>
							)
						)}
					</div>
				</section>
				<Modal isOpen={isOpen} closeModal={closeModal} ></Modal>
				<ContinueButton
					isDisabled={false}
					title={"Continuar"}
					nextPage={() => {
						console.log("nextPage");
						openModal()
					}}
				/>

				<OutlineButton></OutlineButton>
                <br></br>
    		</Layout>);
}

export default FormDataPM;