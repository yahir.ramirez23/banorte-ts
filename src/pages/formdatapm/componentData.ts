const ComponentData = {
	columns: 2,
	components: [
		{
			idPregunta: 0,
			id: "21",
			columns: 2,
			children: [
				{
					idPregunta: 2,
					label: "¿Qué relación tienes con la empresa?",
					id: "3",
					name: "typeralation",
					options: [
						{
							key: "1",
							value: "uno",
						},
						{
							key: "2",
							value: "dos",
						},
					],
				},
				
			],
		},
		{
			idPregunta: 0,
			id: "20",
			columns: 2,
			children: [
				{
					idPregunta: 1,
					type: "text",
					label: "CURP",
					id: "1",
					name: "curp",
				},
				{
					idPregunta: 1,
					type: "text",
					label: "Nombre(s)",
					id: "2",
					name: "name",
				},
				{
					idPregunta: 1,
					type: "text",
					label: "Apellido paterno",
					id: "3",
					name: "apellidop",
				},
				{
					idPregunta: 1,
					type: "text",
					label: "Apellido materno",
					id: "4",
					name: "apelidom",
				},
			],
		},

		
	],
	elements: [
		
		{
			idPregunta: 0,
			id: "20",
			columns: 2,
			children: [
				{
					idPregunta: 1,
					type: "text",
					label: "Ingresa tu numero de celular",
					id: "1",
					name: "cel",
				},
				{
					idPregunta: 1,
					type: "text",
					label: "Ingresa tu correo electrónico",
					id: "2",
					name: "email",
				},
				{
					idPregunta: 1,
					type: "text",
					label: "Confirmar tu numero de celular",
					id: "3",
					name: "confirmationcel",
				},
				{
					idPregunta: 1,
					type: "text",
					label: "Confirma tu correo electrónico",
					id: "4",
					name: "confirmationemail",
				},
			],
		},
		{
			idPregunta: 3,
			content:
				'<p> Al continuar usted otorga el consentimiento para que Banorte de tratamiento a sus datos de acuerdo a lo establecido en su <a style="color:red;" href="https://www.youtube.com/watch?v=AgfhVb0Jbg0"> aviso de privacidad </a> y autoriza a Banorte al uso de su ubicación geográfica mediante el uso de geolocalización',
			id: "6",
			icon:"",
			name: "razonsocial",
		},
		
	],
};

export default ComponentData;
