import ComponentData from "./utils/componentData";
import InfoText from "../../components/InfoText/InfoText";
import Input from "../../components/Input";
import Select from "../../components/Select";
import styles from "./sass/company.module.scss";
import Layout from "../../layout";
import ContinueButton from "../../components/continuebutton";
import BackButton from "../../components/backbutton";
import { ParamComponent } from "../../common/model/ParamComponent";

const CompanyData = () => {
	const { components } = ComponentData;

	const componentSwitch = (component: ParamComponent) => {
		const { idPregunta } = component;

		switch (idPregunta) {
			case 1:
				return (
					<Input
						label={component.label!}
						type={component.type!}
						name={component.name!}
						id={component.id}
					/>
				);
			case 2:
				return (
					<Select
						label={component.label!}
						name={component.name!}
						id={component.id}
						options={component.options ? component.options : []}
					/>
				);
			case 3:
				return (
					<InfoText
						icon={component.icon!}
						content={component.content!}
						id={component.id}
					/>
				);
			default:
				return;
		}
	};

	return (
		<Layout>
			<section className={styles["holder"] + " auto-cols-fr "}>
				<BackButton isDisabled={false} goBack={() => {}} />
				<div className="mt-5">
					{components.map((component) =>
						component.idPregunta !== 0 ? (
							componentSwitch(component)
						) : (
							<section className={`grid grid-cols-${component.columns} gap-4`}>
								{component.children?.map((item) => componentSwitch(item))}
							</section>
						)
					)}
				</div>
			</section>
			<ContinueButton
				isDisabled={false}
				title={"Continuar"}
				nextPage={() => {
					console.log("nextPage");
				}}
			/>
		</Layout>
	);
};

export default CompanyData;
