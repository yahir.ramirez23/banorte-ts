const ComponentData = {
	columns: 2,
	components: [
		{
			idPregunta: 0,
			id: "20",
			columns: 2,
			children: [
				{
					idPregunta: 1,
					type: "text",
					label: "Razón Social",
					id: "1",
					name: "razonsocial",
				},
				{
					idPregunta: 1,
					type: "text",
					label: "RFC",
					id: "2",
					name: "rfc",
				},
			],
		},

		{
			idPregunta: 0,
			id: "21",
			columns: 2,
			children: [
				{
					idPregunta: 2,
					label: "Tipo de teléfono",
					id: "3",
					name: "tipotelefono",
					options: [
						{
							key: "1",
							value: "Celular",
						},
						{
							key: "2",
							value: "Fijo",
						},
					],
				},
				{
					idPregunta: 1,
					type: "tel",
					label: "Teléfono con clave LADA",
					id: "4",
					name: "telefono",
				},
			],
		},

		{
			idPregunta: 0,
			id: "21",
			columns: 2,
			children: [
				{
					idPregunta: 1,
					type: "email",
					label: "Correo Electrónico",
					id: "5",
					name: "razonsocial",
				},
			],
		},

		{
			idPregunta: 3,
			content:
				'<p> Al continuar usted otorga el consentimiento para que Banorte de tratamiento a sus datos de acuerdo a lo establecido en su <a style="color:red;" href="https://www.youtube.com/watch?v=AgfhVb0Jbg0"> aviso de privacidad </a> y autoriza a Banorte al uso de su ubicación geográfica mediante el uso de geolocalización',
			id: "6",
			icon:"",
			name: "razonsocial",
		},
	],
};

export default ComponentData;
