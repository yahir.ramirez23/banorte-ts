import ComponentData from "./utils/components";
import RenderComponent from "../../common/RenderComponent";
import Layout from "../../layout";
import ContinueButton from "../../components/continuebutton";
import BackButton from "../../components/backbutton";

const { components } = ComponentData;

const BusinessQuestions = () => {
	return (
		<Layout>
			<section>
				<BackButton isDisabled={false} goBack={() => {}} />
				<div className="mt-5 p-4">
					{components.map((component) =>
						component.hasOwnProperty("children") ? (
							<section
								className={"grid" + ` grid-cols-${component.columns} gap-4`}>
								{component.label ? (
									<h1
										className={`col-span-${component.columns} font-bold text-lg font-bold`}>
										{" "}
										{component.label}{" "}
									</h1>
								) : null}
								{component.children?.map((item) => (
									<RenderComponent item={item} />
								))}
							</section>
						) : (
							<RenderComponent item={component} />
						)
					)}
				</div>
			</section>
			<ContinueButton
				isDisabled={false}
				title={"Continuar"}
				nextPage={() => {
					console.log("nextPage");
				}}
			/>
		</Layout>
	);
};

export default BusinessQuestions;
