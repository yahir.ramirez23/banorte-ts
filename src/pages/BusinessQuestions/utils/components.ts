const ComponentData = {
	components: [
		{
			idPregunta: 4,
			id: "26",
			label: "¿Cuál de las opciones describe mejor la actividad de tu empresa?",
			columns: 3,
			selectedOption: "agropecuario",
			children: [
				{
					id: "agropecuario",
					name: "businessActivity",
					label: "Agropecuario",
					value: "agropecuario",
					idPregunta: 4,
					type: "radio",
				},
				{
					id: "comercio",
					name: "businessActivity",
					label: "Comercio",
					value: "comercio",
					idPregunta: 4,
					type: "radio",
				},
				{
					id: "manufacturero",
					name: "businessActivity",
					label: "Manufacturero",
					value: "manufacturero",
					idPregunta: 4,
					type: "radio",
				},

				{
					id: "construcción",
					name: "businessActivity",
					label: "Construcción",
					value: "construcción",
					idPregunta: 4,
					type: "radio",
				},
				{
					id: "petroquímico",
					name: "businessActivity",
					label: "Petroquímico",
					value: "petroquímico",
					idPregunta: 4,
					type: "radio",
				},
				{
					id: "other",
					name: "businessActivity",
					label: "Otro Sector",
					value: "other",
					idPregunta: 4,
					type: "radio",
				},
			],
		},
		{
			idPregunta: 4,
			id: "27",
			label: "¿Cuál es el rango actual aproximado de las ventas de tu empresa?",
			columns: 2,
			selectedOption: "agropecuario",
			children: [
				{
					id: "gain100",
					name: "ganancias",
					label: "$100,000 a $5,000,000",
					value: "agropecuario",
					idPregunta: 4,
					type: "radio",
				},
				{
					id: "comercio",
					name: "businessActivity",
					label: "Comercio",
					value: "comercio",
					idPregunta: 4,
					type: "radio",
				},
				{
					id: "manufacturero",
					name: "businessActivity",
					label: "Manufacturero",
					value: "manufacturero",
					idPregunta: 4,
					type: "radio",
				},

				{
					id: "construcción",
					name: "businessActivity",
					label: "Construcción",
					value: "construcción",
					idPregunta: 4,
					type: "radio",
				},
			],
		},
	],
};

export default ComponentData;
