import styles from "./sass/home.module.scss";
import { Link } from "react-router-dom";
const Home = () => {
	return (
		<>
			<div className={styles["mainContent"]}>
				<section className={styles["leftGradient"]}>
					<div className={styles["leftGradient__content"]}>
						<h1 className={"text-3xl font-black	"}>
							Abrir una cuenta con Banca electrónica para tu negocio nunca habia
							sido tan sencillo
						</h1>
						<p className="p-5 mb-1 mt-1">
							{" "}
							Ahora puedes solicitar tu cuenta en línea de forma rápida sin
							acudir a una sucursal{" "}
						</p>
					</div>
					<div className={styles.leftGradient__buttons}>
						<ul>
							<li>
								<Link
									to="/welcome"
									className={
										styles["itemBtn"] +
										" bg-red-700 " +
										" mb-6 " +
										" text-white "
									}>
									Comenzar
								</Link>
							</li>
							<li>
								<Link
									to="/welcome"
									className={
										styles["itemBtn"] +
										" border-red-700 " +
										" border-4 " +
										"text-red-700"
									}>
									Continuar contratación *
								</Link>
							</li>
						</ul>
						<p className="mt-3 pl-56 pr-56">
							* Si no finalizas tu contratación puedes retomarla en cualquier
							momento
						</p>
					</div>
				</section>
			</div>
		</>
	);
};

export default Home;
