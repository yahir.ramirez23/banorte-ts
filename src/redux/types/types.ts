export const ADD_ACTION = "ADD_ACTION";
export const SUBSTRACT_ACTION = "SUBSTRACT_ACTION";
export const RESET_ACTION = "RESET_ACTION";

export interface PostState {
	counter: number;
}

export interface BaseAction {
	type: typeof ADD_ACTION | typeof SUBSTRACT_ACTION | typeof RESET_ACTION;
	payload?: any;
}

export interface TypeReducer {
	ADD_ACTION?: PostState;
	SUBSTRACT_ACTION?: PostState;
	RESET_ACTION?: PostState;
}
