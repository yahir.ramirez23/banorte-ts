import { PostState, BaseAction, TypeReducer } from "../../types/types";

const initialState: PostState = {
	counter: 0,
};

export default (state = initialState, action: BaseAction) => {
	const { type, payload } = action;

	const REDUCER: TypeReducer = {
		// Borrar Image
		ADD_ACTION: {
			counter: state.counter + payload,
		},
		SUBSTRACT_ACTION: {
			counter: state.counter - payload,
		},
		RESET_ACTION: {
			counter: 0,
		},
	};

	return { ...state, isLoading: false, ...REDUCER[type] };
};
