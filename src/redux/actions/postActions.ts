import {
	BaseAction,
	ADD_ACTION,
	SUBSTRACT_ACTION,
	RESET_ACTION,
} from "../types/types";

export { ADD_ACTION, SUBSTRACT_ACTION, RESET_ACTION };

export const addAction = (payload: number = 1): BaseAction => ({
	type: ADD_ACTION,
	payload,
});

export const substractAction = (payload: number = 1): BaseAction => ({
	type: SUBSTRACT_ACTION,
	payload,
});

export const resetAction = (payload: number): BaseAction => ({
	type: RESET_ACTION,
	payload,
});
