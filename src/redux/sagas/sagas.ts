import { all, put, takeLatest } from "redux-saga/effects";
import {
	ADD_ACTION,
	SUBSTRACT_ACTION,
	RESET_ACTION,
	addAction,
	substractAction,
	resetAction,
} from "../actions/postActions";
import { BaseAction } from "../types/types";

function* addSaga() {
	/* yield put(addAction(2)); */
	console.log("sumamos");
}

function* substractSaga() {
	console.log("restamos");
}

function* resetSaga() {
	console.log("reseteamos");
}

export function* sumButtonSaga() {
	yield all([takeLatest(ADD_ACTION, addSaga)]);
}

export function* restButtonSaga() {
	yield all([takeLatest(SUBSTRACT_ACTION, substractSaga)]);
}

export function* resetButtonSaga() {
	yield all([takeLatest(RESET_ACTION, resetSaga)]);
}
