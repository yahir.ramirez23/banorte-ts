import { all, fork } from "redux-saga/effects";
import * as mainSagas from "./sagas";

export function* rootSaga() {
	yield all([...Object.values(mainSagas)].map(fork));
}
