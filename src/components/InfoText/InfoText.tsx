interface Props {
	content: string;
	icon: string;
	id: string;
}

const InfoText = ({ content, icon, id }: Props) => {
	return (
		<>
			<div
				className="w-10/12 grid grid-cols-4 bg-gray-300 p-4 mx-auto"
				key={id}>
				<div className="col-span-1">{icon}</div>
				<div
					className="col-span-3 px-4"
					dangerouslySetInnerHTML={{ __html: content }}
				/>
			</div>
		</>
	);
};

export default InfoText;
