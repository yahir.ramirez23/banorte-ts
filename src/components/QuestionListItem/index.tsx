import { ParamComponent } from "../../common/model/ParamComponent";
import RenderComponent from '../../common/RenderComponent'
interface Props {
	idPregunta?: number;
	question: string;
	columns: number;
	visibleLabels: boolean;
	children: ParamComponent[];
}

function QuestionListITem({
	idPregunta,
	question,
	columns,
	visibleLabels,
	children,
}: Props) {
	return (
		<>
		   <li className={`grid grid-cols-${columns}`}>
               {children.map(item)=>(
                 <h3> {question} </h3>
                 <RenderComponent 
                   children
                 />
               )}
           </li>
		</>
	);
}

export default QuestionListITem;
