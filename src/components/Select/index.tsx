import styles from "./sass/select.module.scss";

interface SelectOptions {
	key: string;
	value: string;
}

interface Props {
	label: string;
	options: SelectOptions[];
	name: string;
	id: string;
}

const Input = ({ label, options, name, id }: Props) => {
	return (
		<>
			<article className={styles["selectArticle"]}>
				<label className={styles["selectLabel"] + " mb-2 text-left "}> {label} </label>
				<div className={styles["arrowContent"]}>
					<i className={styles["arrow"]} />
				</div>
				<select
					name={name}
					id={id}
					className="border-solid border-2 border-black p-2 appearance-none block w-full">
					{options.map((item) => (
						<option value={item.key}> {item.value} </option>
					))}
				</select>
			</article>
		</>
	);
};

export default Input;
