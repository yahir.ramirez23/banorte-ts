
import styles from './headeroptions.module.scss';
type Props = {
    title: string,
    indice: number
}

function HeaderOptions({title,indice}:Props) {
    return (<div className="flex items-center">
                <div className={"shadow-inner "+styles.circle}>{indice}</div>
                <div className={"text-left " +styles.text}>{title} </div>
            </div>);
}

export default HeaderOptions;