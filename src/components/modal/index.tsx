import { Dialog, Transition } from '@headlessui/react'
import { Fragment, useState } from 'react'
import ContinueButton from '../continuebutton'
import Input from '../Input'
import styles from './Modal.module.scss'

interface Props {
    isOpen: boolean;
    closeModal:()=>void;
}

export default function Modal({isOpen,closeModal}: Props) {
  return (
    <>
      <Transition appear show={isOpen} as={Fragment}>
        <Dialog as="div" className="relative z-10" onClose={closeModal}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black bg-opacity-25" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto">
            <div className="flex min-h-full items-center justify-center p-4 text-center">
              <div className={styles["closeButton"]}>
              <button
                      type="button"
                      className={styles["button"]}
                      onClick={closeModal}
                    >
                      x
                    </button>

              </div>
          
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-md bg-white p-6 text-left align-middle shadow-xl transition-all">
              
                  <Dialog.Title
                    as="h3"
                    className="text-lg font-medium leading-6 text-gray-900 text-center"
                  >
                      
                    Ingresa el codigo de verificacion que recibiste por SMS al número
                    ***7230
                  </Dialog.Title>
                  <div className="mt-2 text-center">
                  <Input type="number" label="" name="codigo"  id="codigo"></Input>
                  <p>El codigo que ingresaste es incorrecto</p>
              
                  <h2 className="flex justify-center items-center">Vence en <div className={styles.circle}><p>3:00 </p> </div> minutos</h2>
                  <p>¿No lo recibiste?</p>
                  <a>Renviarlo aquí</a>
                  </div>
                  <div className="mt-4 flex justify-center">
                      <ContinueButton isDisabled={false} title="Continuar" nextPage={()=>{}} ></ContinueButton>
                    
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </>
  )
}
