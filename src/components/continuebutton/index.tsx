import styles from './continuebutton.module.scss';
type Props = {
  isDisabled: boolean
  nextPage:()=>void
  title?:string  
}
function ContinueButton({nextPage,isDisabled,title="Continue"}:Props) {
    return (<button 
                className={"rounded-lg mt-5 "+styles['buttom']} 
                style={{backgroundColor:isDisabled ?"rgba(236, 236, 236, 0.979)":"rgb(228, 18, 18)"}}
                onClick={nextPage}
                disabled={isDisabled}
                >{title}
            </button> 
        );
}

export default ContinueButton;