import styles from "./sass/input.module.scss";

interface Props {
	label: string;
	type: string;
	name: string;
	id: string;
}

const Input = ({ label, type, name, id }: Props) => {
	return (
		<>
			<article>
				<label className={styles["inputLbl"] + " mb-2 text-left "}> {label} </label>
				<input
					type={type}
					name={name}
					id={id}
					className="border-solid border-2 border-black p-2 w-full mb-3"
				/>
			</article>
		</>
	);
};

export default Input;
