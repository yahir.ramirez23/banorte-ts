
import styles from './backbutton.module.scss'

type Props = {
    goBack:()=>void
    isDisabled?: boolean
}

function BackButton({goBack, isDisabled}:Props) {
    return (<button className={"flex rounded-full justify-center "+ styles["borderButton"]} onClick={goBack} disabled={isDisabled}>
                <img width="20" src="icons/back.svg"/><p>Regresar</p>
            </button>);
}

export default BackButton;