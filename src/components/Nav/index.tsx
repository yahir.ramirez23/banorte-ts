import { Link } from "react-router-dom";

const Navigator = () => {
	return (
		<nav className="columns-4">
			<Link to="/"> Home </Link>
			<Link to="/registro"> Registro </Link>
		</nav>
	);
};

export default Navigator;
