import styles from "./sass/radioBtn.module.scss";
interface Props {
	label: string;
	type: string;
	name: string;
	id: string;
	value: string;
	selectedOption?: string;
	changeFunction?: () => void;
}

const InputRadio = ({
	label,
	type,
	name,
	id,
	value,
	selectedOption,
	changeFunction,
}: Props) => {
	return (
		<>
			<article className={styles["radioHolder"] + " p-2 " + " w-36"}>
				<input
					type={type}
					id={id}
					name={name}
					value={value}
					onChange={() => {
						changeFunction();
					}}
				/>

				<label htmlFor={id} className={"p-4"}>
					{" "}
					{label}{" "}
				</label>
			</article>
		</>
	);
};

export default InputRadio;
