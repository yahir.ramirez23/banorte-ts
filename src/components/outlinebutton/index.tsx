
import { useState,useTransition,useId} from "react";

function OutlineButton() {
  const [isPending, startTransition] = useTransition();
  const [count, setCount] = useState(0);
  const id = useId();
  console.log("id::",id)
  function handleClick() {
    startTransition(() => {
      setCount(c => c + 1);
    })
  }

  return (
    <div>
      {isPending && <p>cargando....</p> }
      <button onClick={handleClick}>{count}</button>
    </div>
  );
}

export default OutlineButton;


