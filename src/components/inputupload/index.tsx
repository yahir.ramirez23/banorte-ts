import styles from './inputupload.module.scss';
interface InputUploadProps {    
}
const InputUpload = () => {
    return (
    <div className={styles["button"]}>
        <img src="icons/upload.png" width="10px" height="10px"></img> 
        
        <input type="file"
        id="avatar" name="avatar"
        accept="image/png, image/jpeg" className={styles["custom-file-input"]}></input>  
    </div>
     );
}
 
export default InputUpload;