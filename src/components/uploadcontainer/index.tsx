import styles from './uploadcontainer.module.scss';
import InputUpload from "../inputupload";

function UploadContainer() {
    return ( <div className={styles["container"]}>
                <div className={styles["upload"]}>
                    <img src="icons/google-docs.png" className={styles["icons"]}></img>
                    <div className={styles["input"]}>
                        <InputUpload></InputUpload>
                    </div>
                </div>
            </div> );
}

export default UploadContainer;