
import HeaderOptions from '../components/headeroptions'
import styles from './layout.module.scss'
type Props = {
   children:JSX.Element | JSX.Element[]
}
function Layout({children}:Props) {
    return (<div  className="justify-center">
                <div className="flex justify-center h-16 " style={{backgroundColor:"gray"}}>
                    <div  className="container w-2/3 px-2 grid grid-cols-6 gap-4 items-center text-white"> 
                        <h2 className="col-span-4 text-left font-bold">APERTURA DE CUENTA EMPRESARIAL</h2>  
                        <div className={"col-span-1 rounded-full "+styles.buttom_ayuda} >Ayuda</div> <div className="col-span-1">Salir</div>
                    </div>
                </div>

                <div className="flex justify-center h-16" style={{backgroundColor:"#dadada"}}>
                    <div  className="container w-2/3  grid grid-cols-5 gap-1 items-center text-white">                     
                        <div className="col-span-1"><HeaderOptions indice={1} title="Elige una cuenta"/></div> 
                        <div className="col-span-1"><HeaderOptions indice={2} title="Ingresa los datos del titular"/></div>
                        <div className="col-span-1"><HeaderOptions indice={3} title="Ingresa los datos de la empresa"/></div>
                        <div className="col-span-1"><HeaderOptions indice={4} title="Configura tu cuenta"/></div>
                        <div className="col-span-1"><HeaderOptions indice={5} title="Finalizar"/></div>
                    </div>
                </div>

                <div className="flex justify-center "style={{backgroundColor:"#dadada"}}>
                    <div  className="container w-2/3  rounded-lg" style={{backgroundColor:"white"}}> 
                      
                    {children}
                    </div>
                </div>
       
    </div>  );
}

export default Layout;