type options = {
	key: string;
	value: string;
};

export interface ParamComponent {
	idPregunta: number;
	content?: string;
	label?: string;
	columns?: number;
	id: string;
	icon?: string;
	type?: string;
	name?: string;
	options?: options[];
	value?: string;
	selectedOption?: string;
	changeFunction?: void;
}
