import InfoText from "../../components/InfoText/InfoText";
import Input from "../../components/Input";
import Select from "../../components/Select";
import InputRadio from "../../components/InputRadio";
import { ParamComponent } from "../model/ParamComponent";

interface Props {
	item: ParamComponent;
}

const RenderComponent = ({ item }: Props) => {
	const componentSwitch = (component: ParamComponent) => {
		const { idPregunta } = component;

		switch (idPregunta) {
			case 1:
				return (
					<Input
						label={component.label!}
						type={component.type!}
						name={component.name!}
						id={component.id}
					/>
				);
			case 2:
				return (
					<Select
						label={component.label!}
						name={component.name!}
						id={component.id}
						options={component.options ? component.options : []}
					/>
				);
			case 3:
				return (
					<InfoText
						icon={component.icon!}
						content={component.content!}
						id={component.id}
					/>
				);
			case 4:
				return (
					<InputRadio
						label={component.label ? component.label : ""}
						type={component.type!}
						id={component.id}
						name={component.name!}
						value={component.value!}
					/>
				);
			default:
				return;
		}
	};

	return <>{componentSwitch(item)}</>;
};

export default RenderComponent;
