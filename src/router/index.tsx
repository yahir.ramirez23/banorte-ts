import { Routes, Route } from "react-router-dom";
import Home from "../pages/Home/Home";
import Registro from "../pages/Registro/Registro";
import WelcomeUser from "../pages/welcomeuser/WelcomeUser";
import CompanyData from "../pages/CompanyData";
import FormDataPM from "../pages/formdatapm";
import BusinessQuestions from "../pages/BusinessQuestions";
const Router = () => {
	return (
		<>
			<Routes>
				<Route path="/" element={<Home />} />
				<Route path="/registro" element={<Registro />} />
				<Route path="/welcome" element={<WelcomeUser />} />
				<Route path="/companydata" element={<CompanyData />} />
				<Route path="/formdatapm" element={<FormDataPM />} />
				<Route path="/businessquestions" element={<BusinessQuestions />} />
			</Routes>
		</>
	);
};

export default Router;
